# README #


### Minesweeper ###
Simple minesweeper game with AI, made on Java Script over Phaser.js framework.

* Version 1.0

* Game allows you to specify dificulty by setting up ratio between mined cells and total cells count. Its also possible to chage total count of cells by changing rows and cols. Those settings can be done through settings screen. You can access it by clicking on a 'gear sight'.
* Game can be restarted from settings screen too. If you loose, you need to go into settings screen and press 'start a new game'. Game starts as document finishs its loading.


### Setup ###
* Clone or download this project
* Run it on your web server

### Notes ###
Sources are split-up onto few files:
 
* gameConfig.js:
All settings of game are listed in gameConfig.js
The most usefull option is debugMines. Set this to true in order to see mines.
 
* mineField.js
All game logic is implemented in mineField.js
 
* guiBuilder.js
Contains helpers and my UI primitives that are used in game
 
* playerController
Is a code base for AI and player interaction
 
* game.js
Is a main module that inits every object and build all primitives