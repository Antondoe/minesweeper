/**
 * This class allows to create various of controllers
 * than can be used to play in a game. A good example is
 * AI controller, that can be a bridge between bot and a game
 * perfectly, player shouldn't know anything about a game's inner
 * structure and works only
 * with game's image
 */
CreatePlayerController = function (gameContext) {
    this.onClick = function (e) {
    var pos = gameContext.getMineFieldLocation();
        var posX = e.x - pos.x + gameOptions.tileSize * gameOptions.fieldWidth / 2;
        var posY = e.y - pos.y + gameOptions.tileSize * gameOptions.fieldHeight / 2;

        // transforming coordinates into actual rows and columns
        var i = Math.floor(posX / gameOptions.tileSize);
        var j = Math.floor(posY / gameOptions.tileSize);

        if (e.leftButton.isDown) {
            gameContext.openCell(i,j);
        }

        if (e.rightButton.isDown) {
            gameContext.toggleFlag(i, j);
        }
    }

    this.onTick  = function(e){

    }

}

/**
 * Strategy to make a turn. can be derived. must implement method
 * makeTurn
 * @param playerController is player controller
 * @constructor
 */
CreateStrategy = function (playerController) {
    var firstTurn = true;
    var image   = null; //game image that player see //0-free; 1-9 mines count; 9 = flagged; -1 = closed
    var width   = 0;
    var height  = 0;
    var controller = playerController;
    var flagsAvailable = 0;

    var toggleFlag = function (cell) {
        if (controller.hasFlags()) {

            controller.toggleFlag(cell.x,cell.y);
            console.log("toggle: " + cell.x + "/" + cell.y +
                ":" + cell.probability);
        } else
            forceSetFlag(cell);

    }
    toggleFlag = toggleFlag.bind(this);

    /**
     * Strategy uses this behaviour
     * when there is no available flags left.
     * The idea is to find flagged cell that is most
     * probably is free cell and remove flag from that
     * cell. Then flag on cell passed as param
     * @param cell to where strategy must set
     * flag
     */
    var forceSetFlag = function (cell) {
        var variants = getVariants(9);
        var cellWeak = variants[0];
        controller.toggleFlag(cellWeak.x, cellWeak.y); //remove flag from most likely empty cell
        controller.openCell(cellWeak.x, cellWeak.y);
        console.log("toglle: " + cellWeak.x + "/" + cellWeak.y +
            ":" + cellWeak.probability);
        controller.toggleFlag(cell.x, cell.y);
    }
    forceSetFlag = forceSetFlag.bind(this);

    /**
     * This strategy searches for cells with
     * probability of be mined equals to 0 then open them
     * If there is no cell with that condition, then
     * it search for cells that must contain a mine with
     * probability = 1. if both conditions not carried
     * then it opens cell with smallest probability or
     * flags cell with the most high probability. it depends
     * on which event is more affordable.
     * if there is no flags available, it uses case implemented
     * in forceSetFlag method
     *
     * @param gameImage
     * @param flagsAvailableCount
     * @param firstTurn
     */
    this.makeTurn = function (gameImage, flagsAvailableCount) {

        image = gameImage;
        width = image.length;
        height = image[0].length;
        var variants = getVariants(-1); //get all unknown cells
        flagsAvailable = flagsAvailableCount;

        if (firstTurn){
            var r = Math.random();
            var cell = variants[Math.floor((variants.length-1)*r )];
            controller.openCell(cell.x,cell.y);
            firstTurn = false;
            return;
        }
        if (variants.length > 0) {
            var mostProbablyMinedCell = variants[variants.length - 1];
            var mostProbablyFreeCell = variants[0];

            if (mostProbablyMinedCell.probability >= 1) {
                toggleFlag(mostProbablyMinedCell);
                return;
            }

            if (mostProbablyFreeCell.probability <= 0) {
                controller.openCell(mostProbablyFreeCell.x,
                    mostProbablyFreeCell.y);
                console.log("open: " + mostProbablyFreeCell.x + "/" + mostProbablyFreeCell.y +
                    ":" + mostProbablyFreeCell.probability);
            }

            if ((1 - mostProbablyFreeCell.probability) < mostProbablyMinedCell) {
                toggleFlag(mostProbablyMinedCell);
                return;
            }
            else {
                if (mostProbablyFreeCell.probability > 0 && controller.hasFlags()) {
                    toggleFlag(mostProbablyFreeCell);
                    return;
                }

                controller.openCell(mostProbablyFreeCell.x,
                    mostProbablyFreeCell.y);
                console.log("open: " + mostProbablyFreeCell.x + "/" + mostProbablyFreeCell.y +
                    ":" + mostProbablyFreeCell.probability);
            }
        }
    }

    /**
     * Evaluate probs for cells of specified type and
     * sort it
     * type: 9  = flagged cells
     * type: -1 = closed cells
     * @param type
     * @returns {Array}
     */
    var getVariants = function (type){
        var variants = [];
        for (var i=0; i< width; i++)
            for (var j =0; j< height; j++) {
                if (image[i][j] === type) {
                    if (type === 9) image[i][j] =-1; //flag don'e affect on nearby cells
                    var testCell = new CreateNewCell(i, j);
                    testCell.probability = testCell.getMineProbability();
                    variants.push(testCell);
                    image[i][j] = type;
                }
            }
        variants.sort(function(a,b){return a.probability>b.probability});
        return variants;
    }
    getVariants = getVariants.bind(this);

    var checkBounds =  function (i,j) {return  (i>=0 && j>=0 && i< width && j < width )? true : false;}
    checkBounds = checkBounds.bind(this);
    /**
     * Helper object
     * @param i cell x position in game image
     * @param j cell y position in game image
     * @constructor
     */
    var CreateNewCell = function (i,j){
        this.x = i;
        this.y = j;
        var value = image[i][j];

        /**
         * returns number of mines around this cell
         *  @returns {number}
         */
        this.getValue = function() {
            if (value === -1) return -1;
            if (value === 9) return 9;
            if (value === 0) return 0;
            var nearbyCellsCache = getCellsAround(this.x, this.y);
            var closedCellsCount = 0;
            var flaggedCellsCount = 0;
            var freeCells = 0;

            for (i = 0; i < nearbyCellsCache; i++) {
                var cell = nearbyCellsCache[i];

                if (cell.getValue() === -1) {
                    freeCells++
                }

                if (cell.getValue() === 9) {
                    closedCellsCount++;
                    continue;
                }   //0 free // 1-8 distance //9 flagged //-1 closed
            }
            if (freeCells ==0) freeCells =1;
            var val  = (value - flaggedCellsCount) / freeCells;
            return val;
        };

        /**
         * returns a value what means how strong
         * this cell is affected on probability of be
         * mined for mines around
         * @returns {number}
         */
        this.getWeight = function() {
            if (value === -1)
                return 0;
            var nearbyCellsCache = getCellsAround(this.x,this.y);

            //var nearbyCellsCache = getCellsAround(this.x,this.y);
            var flaggedCellsCount = 0;
            var unknownCount = 0;
            for (var i=0; i< nearbyCellsCache.length; i++){
                if (nearbyCellsCache[i].getValue() === -1 ) unknownCount++;
                if (nearbyCellsCache[i].getValue() === 9  ) flaggedCellsCount++;
            }

            if (value === 9) {
                return(1/(unknownCount));
            }
            var val = this.getValue();

            if (flaggedCellsCount == val) return -1;  // cell cant have mines anymore
            return val / (unknownCount-1);            // cell can have mine around
        };
        /**
         * This function calculates probability
         * to be mined for some cell. !This method is not
         * mathematically correct.
         * @returns {number}
         */
        this.getMineProbability = function() {
            var mineProb = 0;
            var nearbyCells = getCellsAround(this.x, this.y);
            var unknown = 1;
            for (var i = 0; i < nearbyCells.length; i++) {
                var cell = nearbyCells[i];
                if (cell.value === -1) unknown++;
                mineProb = mineProb += cell.getWeight();
            }
            var val = mineProb / (nearbyCells.length);
            return val;
        }
    }
    CreateNewCell = CreateNewCell.bind(this)
    /**
     * Helper to get cells around some given cell's coordinates
     * @param i x cell position
     * @param j y cell position
     * @returns {Array}
     */
    var getCellsAround = function (i,j){
        var res = [];
        if (checkBounds(i-1 ,j  )) res.push(new CreateNewCell (i-1,j  ));
        if (checkBounds(i-1 ,j-1)) res.push(new CreateNewCell (i-1,j-1));
        if (checkBounds(i   ,j-1)) res.push(new CreateNewCell (i  ,j-1));
        if (checkBounds(i+1 ,j-1)) res.push(new CreateNewCell (i+1,j-1));
        if (checkBounds(i+1 ,j  )) res.push(new CreateNewCell (i+1,j  ));
        if (checkBounds(i+1 ,j+1)) res.push(new CreateNewCell (i+1,j+1));
        if (checkBounds(i   ,j+1)) res.push(new CreateNewCell (i  ,j+1));
        if (checkBounds(i-1 ,j+1)) res.push(new CreateNewCell (i-1,j+1));
        return res;
    };
    getCellsAround = getCellsAround.bind(this);

}

/**
 *  Creates AI Controller
 * @param gameContext
 * @constructor
 */
CreateAIController = function (gameContext) {
    CreatePlayerController.apply(this, arguments);
    var strategy = new CreateStrategy(this);
    var image = [];
    this.hasFlags = function () {
        return gameContext.getFlagsCount();
    }

    this.openCell = function(i,j){
        gameContext.openCell(i,j);
    }

    this.toggleFlag = function(i,j){
        gameContext.toggleFlag(i,j);
    }


    this.onTick = function (e) {
        image = gameContext.getGameImage();
        strategy.makeTurn(image,gameContext.getFlagsCount());
    }

    this.onClick = function(e){return}

}

CreateAIController.prototype = Object.create(CreatePlayerController.prototype);
CreateAIController.prototype.constructor = CreateAIController;
