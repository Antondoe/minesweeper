/**
 * This object contains all customizable game options
 * changing them will affect gameplay
 */

var gameOptions = {

    /* Gameplay config------------*/
    debugMines          : false,   // set to true to see where mines are planted
    gameWidth           : window.outerWidth*2,
    gameHeight          : window.outerHeight*2,
    tileSize            : 64,     // tile size of cell, in pixels

    fieldWidth          : 9,      // count of cells on mine field (x axe)
    fieldHeight         : 9,      // count of cells (y axe)
    minFieldWidth       : 9,      // user can't create field with width less than this
    maxFieldWidth       : 12,     // max cols count in mineFiled (can be adapted to fill whole space on window)

    minFieldHeight      : 9,
    maxFieldHeight      : 12,

    //difficulty
    minesRatio          : 0.1,   // ratio between mined and free cells (default is 0.33; ~ 1/3 of cells are mined)
    minesRatioMax       : 0.6,    // max ratio
    minesRatioMin       : 0.1,    // min ratio

    /*GUI config------------------*/

    //digital display
    digitalDisplaySpriteName : "numbers",
    timerDigits         : 3,
    timerWidth          : 64,
    timerHeight         : 96,
    digitalDisplayRatio : 0.66,

    menuButtonWidth     : 96,
    botButtonWidth      : 96,
    optionsButtonWidth  : 96,

    //wide button
    wideButtonSpriteName : "wideButton",
    wideButtonWidth      : 256,
    wideButtonHeight     : 96,

    //square button
    shortButtonSpriteName : "shortButton",
    shortButtonWidth      : 48,
    shortButtonHeight     : 48,

    //window
    windowBackgroundSpriteName : "background",
    windowWidth                : 512,
    windowHeight               : 512,

    /**
     * This function calculates maxFiledWidth and MaxFieldHeight so that mine field can fill whole
     * space in window
     */
    updateMaxSizeOfMineField : function () {

        this.maxFieldWidth   = Math.floor((this.gameWidth  - this.tileSize*2) / this.tileSize); // all available space
        this.maxFieldHeight  = Math.floor((this.gameHeight -this.timerHeight*2) / this.tileSize); //all available space
    },

    /**
     * Helper to load resources
     */
    loadResources : function () {
        game.load.image("closed", "assets/sprites/closed.png");
        game.load.image("flagged", "assets/sprites/flagged.png");
        game.load.image("opened", "assets/sprites/opened.png");
        game.load.image("mined", "assets/sprites/mined.png");
        game.load.image("t1", "assets/sprites/1.png");  //it will be nice to pack this into atlas
        game.load.image("t2", "assets/sprites/2.png");
        game.load.image("t3", "assets/sprites/3.png");
        game.load.image("t4", "assets/sprites/4.png");
        game.load.image("t5", "assets/sprites/5.png");
        game.load.image("t6", "assets/sprites/6.png");
        game.load.image("t7", "assets/sprites/7.png");
        game.load.image("t8", "assets/sprites/8.png");
        game.load.image("next", "assets/sprites/next.png");
        game.load.image("prev", "assets/sprites/prev.png");
        game.load.image(gameOptions.windowBackgroundSpriteName, "assets/sprites/background.png");
        game.load.spritesheet("numbers",    "assets/sprites/numbers.png",13,23);
        game.load.spritesheet(gameOptions.shortButtonSpriteName, "assets/sprites/shortButton.png",23,23);
        game.load.spritesheet("options", "assets/sprites/options.png",23,23);
        game.load.spritesheet(gameOptions.wideButtonSpriteName , "assets/sprites/wideButton.png" ,130,23);
        game.load.spritesheet("bot" , "assets/sprites/bot.png" ,23,23);

    }


};