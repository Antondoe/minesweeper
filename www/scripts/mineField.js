/**
 * Core object that maintains all operations with mineGrid
 * This object is a model of a game. the view is declared
 * in guiBuilder.js and constructed by calling CreateMineFieldView
 */

/**
 * Constructor that builds one cell of mine field. this object
 * is a part of MineFiled (see function MainField)
 *
 * @param xPos x position of cell inside mineField
 * @param yPos y position of cell inside mineField
 * @param hasMine indicates that cell is mined
 * @param minesCountAround is a numer of mined cells around
 * @constructor
 */
function MineFieldCell (xPos, yPos, hasMine, minesCountAround) {

    this.nearbyMinesCount = minesCountAround;      // 1-8: for count, 0: for free cell, -1: for unknown
    var flagged        = false;
    var opened         = false;                 // indicates that cell was opened by a player
    this.mined         = hasMine ? true : false;

    this.xPosition     = xPos     ;  //x position in minefiled
    this.yPosition     = yPos     ;  //y position in minefield

    this.open       = function ()             {opened = true;}
    this.toggleFlag = function ()             {flagged = !flagged;}
    this.isOpened   = function ()             {return opened  }
    this.isEmpty    = function ()             {return this.nearbyMinesCount ==  0 && !this.mined }
    this.isFlagged  = function ()             {return flagged }
    this.isMined    = function ()             {return this.mined}
    this.getNearbyMinesCount = function()     {return this.nearbyMinesCount}
    
}
/**
 * Construct MineField object, the core object in a game
 * this object owns it's view, that is constructed by calling CreateMineFieldView
 *
 * @param gameContext is Phaser game object
 * @param onFlagsCountChanged is event that should be fired when count of flags is changed
 * @param onCellOpened is event that reports the result of user's action
 * this event pass the result of open/flag action. That way game knows if player won or lose
 * @param callbackContext is object for events will be called (the game)
 * @constructor
 */
function MineField(gameContext,onFlagsCountChanged, onCellOpened, callbackContext) {
    var width            = 0         ; // stores count of cells
    var height           = 0         ; // stores count of cells
    var minesRatio       = 0         ; // ratio : mined cells / count of all cells
    var score            = 0         ; // game's score;
    var gameView         = []        ; // private array representing a minefiled
    var gameImage        = []
    var firstMove        = true      ;
    var _minesCount      = 0         ;
    var flagsCount       = 0         ;
    var freeCellsCount   = 0         ; // to easy check if game was solved
    var spriteGroup      = null      ; // group containing view for mineField

    this.getX = function () {return spriteGroup.x};         //xPosition on a screen
    this.getY = function () {return spriteGroup.y};         //yPosition on a screen


    var updateFreeCellsCount = function () {freeCellsCount = width*height - _minesCount}
    updateFreeCellsCount = updateFreeCellsCount.bind(this);

    var checkBounds = function (i,j) {return (i < width && j < height && i >= 0 && j >= 0);}
    checkBounds = checkBounds.bind(this);

    this.getGameImage = function() {return gameImage}

    /**
     * This function reinitialize the mine field
     */
    this.restart = function () {
        gameView    = [];
        firstMove   = true;
        _minesCount = 0;
        gameImage = [];
        score = 0;
        width  = gameOptions.fieldWidth;
        height = gameOptions.fieldHeight;
        minesRatio = gameOptions.minesRatio;

        for (var i = 0; i < width; i++) {
            gameImage[i]= [];
            if (gameView[i] ===undefined)
                gameView [i] = [];
            for (var j = 0; j < height; j++) {
                gameImage[i][j] = -1;                               // -1 means that cell is closed
                var rndFloat = game.rnd.realInRange(0,1);
                var shouldMountMine = rndFloat <= minesRatio  ;
                if (gameView[i][j] === undefined || shouldMountMine)
                    gameView[i][j] = new MineFieldCell(i, j, shouldMountMine,0);

                gameImage[i][j] = -1;
                _minesCount+=shouldMountMine?1:0;

                if (shouldMountMine)
                    updateNearbyCellsDistancesToMine(i,j);
            }
        }
        flagsCount = _minesCount;
        rebuildView();
        updateFreeCellsCount();
        onFlagsCountChanged.call (gameContext);
    }
    
    /**
     * Rebuilds view for this model
     */
     var rebuildView = function() {
        if (spriteGroup!==null) {
            spriteGroup.destroy(true);
           // spriteGroup.killAll();
        }
        spriteGroup = new CreateMineFieldView(gameContext,gameView);
     }
     rebuildView = rebuildView.bind(this);
    
     /**
     * Helper to count all mines around a cell
     * @param i cell x position
     * @param j cell y position
     * @returns {number}
     */
     function countMinesAround (i,j){
        var counter =0;

         function addDistance (x,y) {
             if (checkBounds(x,y)) {if (gameView[x][y].isMined()) counter++;}
         }
         addDistance = addDistance.bind(this);
         addDistance(i-1 ,j  );
         addDistance(i-1 ,j-1);
         addDistance(i   ,j-1);
         addDistance(i+1 ,j-1);
         addDistance(i+1 ,j  );
         addDistance(i+1 ,j+1);
         addDistance(i   ,j+1);
         addDistance(i-1 ,j+1);
         return counter;
     }
     countMinesAround = countMinesAround.bind(this);
    
     /**
     * Update cell's count of nearby mines
     * @param i cell x position
     * @param j cell y position
     */
    function updateNearbyCellsDistancesToMine(i, j) {

        function addDistance (x,y, value) {
            if (x < width && y < height && x >= 0 && y >= 0)
            {

                if (gameView[x] === undefined)
                    gameView[x] = [];
                if (gameView[x][y]=== undefined)
                    gameView[x][y] = new MineFieldCell(x,y,false,0);

                if (gameView[x][y].isMined())
                    return;

                var distance = gameView[x][y].nearbyMinesCount;
                gameView[x][y] = new MineFieldCell(x,y,false,distance + value);

            }
        }
        addDistance = addDistance.bind(this);
        var distance = isCellMined(i,j)? 1:-1;
        addDistance(i-1 ,j  ,distance);
        addDistance(i-1 ,j-1,distance);
        addDistance(i   ,j-1,distance);
        addDistance(i+1 ,j-1,distance);
        addDistance(i+1 ,j  ,distance);
        addDistance(i+1 ,j+1,distance);
        addDistance(i   ,j+1,distance);
        addDistance(i-1 ,j+1,distance);
    }
    updateNearbyCellsDistancesToMine = updateNearbyCellsDistancesToMine.bind(this);

    /**
     * Check if cell with specified position is mined
     * @param i cell x position
     * @param j cell y position
     * @returns {boolean}
     */
    function isCellMined(i,j){
        if (!checkBounds(i,j)) return false;      // if we trying to check a cell
        return gameView[i][j].isMined();          // outside of field it returns false
     }
    isCellMined = isCellMined.bind(this)

    /**
     * This function generates random coordinates and
     * tries to spawn mine in it, if it is
     * possible. if not function generates different
     * coordinates. This method is not determined.
     * */
    function respawnMine( x,y ) {
        var j = gameContext.rnd.integerInRange(0, height - 1);  // generate random mine position
        var i = gameContext.rnd.integerInRange(0, width  - 1);

        var skipCounter = 1000;                                 // number of attempts after that spawing
                                                                // will be refused

        while (isCellMined(i,j)) {                              // search for first random free cell
            j = gameContext.rnd.integerInRange(0, height - 1);
            i = gameContext.rnd.integerInRange(0, width  - 1);
            if (skipCounter-- == 0) return;                     // cant find a place to mine, player will explode
                                                                // probably, mines count = count of cells
        }

        gameView[x][y].mined = false;                           // create empty cell (no mine)
        gameView[i][j].mined = true;                            // create cell with mine

        gameView[x][y].nearbyMinesCount = countMinesAround(x,y);

        updateNearbyCellsDistancesToMine(x,y);                  // update distances
        updateNearbyCellsDistancesToMine(i,j);
    }
    respawnMine = respawnMine.bind(this);

    /**
     * Check if cell with specified position don't
     * have mines around.
     * @param i cell x position
     * @param j cell y position
     * @returns {boolean}
     */
    function isCellEmpty (i,j)   {return gameView[i][j].isEmpty()}
    isCellEmpty = isCellEmpty.bind(this);

    /**
     * Getter for available flags count
     * @returns {number}
     */
    this.getFlagsCount = function () {return flagsCount}

    /**
     * Getter for mines count
     * @returns {number}
     */
    this.getMinesCount = function () {return _minesCount}

    /**
     * Open all mined cells (called if player loses)
     */
    function openMinedCells() {
        for (var i = 0; i < width; i++)
            for (var j = 0; j < height; j++) {
                var cell = gameView[i][j];
                if (cell.isMined()) {
                    cell.open();
                    spriteGroup.updateSprite(cell);
                    if (cell.isFlagged())
                    score ++;
                }
            }
    }
    openMinedCells = openMinedCells.bind(this);

    /**
     * Update view model for specified cell
     * @param i cell x position
     * @param j cell y position
     */
    function updateView(i,j) {
        if (!checkBounds(i,j)) return;
        var cell = gameView[i][j];
        if (cell.isFlagged()) return;
        if (!cell.isOpened()) {

            if (cell.isMined()) {                       // cell was mined, game over
                openMinedCells();
                onCellOpened.call(callbackContext,-1);
                return;
            }

            cell.open();
            gameImage[i][j] = cell.getNearbyMinesCount();
            freeCellsCount--;
            spriteGroup.updateSprite(cell);
            if (isCellEmpty(i, j))               //open nearby cells and do a recursion
            {
                updateView(i - 1, j);
                updateView(i - 1, j - 1);
                updateView(i, j - 1);
                updateView(i + 1, j - 1);
                updateView(i + 1, j);
                updateView(i + 1, j + 1);
                updateView(i, j + 1);
                updateView(i - 1, j + 1);
            }
            var solved = isSolved();
            if (solved) score = _minesCount;
            onCellOpened.call(callbackContext, solved?1:0);

        }
    }
    updateView = updateView.bind(this);

    /**
     * Try to open cell.
     * return false if cell was successfully opened. returns
     * true if cell is mined.
     *
     * @param i cell x position
     * @param j cell y position
     * @returns {*}
     */
    this.openCell = function (i, j) {
        if (!checkBounds(i,j)) return;

        if (isCellMined(i,j) && firstMove) //don't let player explode at first turn
        {
            respawnMine(i,j);
            rebuildView();
        }
        updateView(i,j);
        firstMove = false;
    }

    /**
     * Toggle flag at specified cell. returns true if flag was changed.
     * Otherwise returns false, that mean that player don't have enough
     * flags.
     *
     * @param i cell x position
     * @param j cell y position
     */
    this.toggleFlag = function (i, j) {
        var cell = gameView[i][j];
        if (flagsCount==0 && !cell.isFlagged()) return false; //not enough flags, skipping
        if (!cell.isOpened())
            cell.toggleFlag();
        flagsCount += cell.isFlagged()?-1:1;                  //change flags count
        spriteGroup.updateSprite(cell);
        onFlagsCountChanged.call (callbackContext);           //update cell view
        onCellOpened.call(callbackContext, isSolved()?1:0);
        gameImage[i][j] =  cell.isFlagged()?9:-1;
        return  true;
    }

    /**
     * Check if player has opened all cells and win
     * @returns {boolean}
     */
    var isSolved = function () {return freeCellsCount==0 && flagsCount == 0}
    isSolved = isSolved.bind(this);

    /**
     * Getter for score. returns 0 if game has not finished, otherwise
     * returns count of unmined cells in relation to minesCount;
     * @returns {*}
     */
    this.getScore = function () {
        return score+"/"+_minesCount;
    }


}