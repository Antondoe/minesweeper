/**
 * Various UI stuff, including CreateMineFieldView constructor
 * that builds a view for game's model.
 * */

/**
 * Creates Digital Display component by extending Phaser.Group
 * @param gameContext
 * @param width is a width of one digit
 * @param height is a height of one digit
 * @param digitsCount is max count of digits that display is able to view
 * @param spriteName is spritesheet to use (10 frames with numbers starts from 0)
 */
CreateDigitalDisplay = function (gameContext,width,height,digitsCount,spriteName) {
   Phaser.Group.call(this,gameContext,gameContext.world);
    var value     =  0;  //value to display
    var sprites   = [];
    var self      = this;

    digitsCount = (digitsCount === undefined) || (digitsCount < 0) ?  0: digitsCount;   //  check if passed params
                                                                                        // are correct

    //TODO: create setter ang getter for those values
    width  = (width       === undefined) || (digitsCount < 0) ? 48: width; //hard-coded def values
    height = (height      === undefined) || (digitsCount < 0) ? 96: height;
    this.spriteSheetName = spriteName!==undefined? spriteName.toString() : "";

    for (var i = 0; i < digitsCount; i++) {
        var sprite = gameContext.add.sprite(i * width, 0, this.spriteSheetName, 0);
        sprite.width = width;
        sprite.height = height;
        sprites.push(sprite);
        this.add(sprite);
    }

     /*
     Private function to update display, called from setter
     */
    var updateDisplay = function () {
        var str = Math.abs(value).toString();              //display does't show negative numbers
        var deltaLength = -digitsCount + str.length;
        var digitsOverflow = deltaLength > 0;               //if this flag is true, display will
                                                            //show 99...9 (number is bigger
                                                            //than display can view)

        for (var i = digitsCount - 1; i >= 0; i--) {
            var strIndex = i + deltaLength;

            if (strIndex < 0) {
                sprites[i].loadTexture(self.spriteSheetName, 0, false);
                continue;
            }

            if (digitsOverflow) self.sprites[i].loadTexture(self.spriteSheetName, 10, false)
            else
                sprites[i].loadTexture(self.spriteSheetName, parseInt(str[strIndex]), false);
        }
    }
    updateDisplay = updateDisplay.bind(this);

    //Setter for value
    this.setValue = function(num){
        num = (num!==parseFloat(num))? 0:num;
        value = Math.floor(num); updateDisplay();
    };

    //Getter for value
    this.getValue = function () {return value};
}
CreateDigitalDisplay.prototype = Object.create(Phaser.Group.prototype);
CreateDigitalDisplay.prototype.constructor = CreateDigitalDisplay;

/**
 * Creates new view for minefileed model
 * @param gameContext
 * @param cells
 * @constructor
 */
CreateMineFieldView = function (gameContext,cells) {
    Phaser.Group.call(this, gameContext, gameContext.world);
    console.log("creating view");
    this.width = gameOptions.tileSize * cells.length;
    this.height = gameOptions.tileSize * cells[0].length;
    var sprites = [];
    /**
     * Creates new sprite for specified cell
     * @param cell
     */
    var createCellSprite = function (cell) {
        var tileXPos = cell.xPosition * gameOptions.tileSize + gameOptions.tileSize / 2;
        var tileYPos = cell.yPosition * gameOptions.tileSize + gameOptions.tileSize / 2;

        if (sprites[cell.xPosition] === undefined) sprites[cell.xPosition] = [];

        var sprite = gameContext.add.sprite(tileXPos,
            tileYPos, "closed"); //TODO: remove this hard-coded into settings

        if (gameOptions.debugMines && cell.isMined()) sprite.loadTexture("mined")
        sprites[cell.xPosition][cell.yPosition] = sprite;
        sprite.anchor.set(0.5);

        sprite.width = gameOptions.tileSize;
        sprite.height = gameOptions.tileSize;

        this.add(sprite);
    };
    createCellSprite = createCellSprite.bind(this);
    /**
     * Update view for specified cell
     * @param cell
     * @returns {*}
     */
    this.updateSprite = function (cell) {
        var sprite = sprites[cell.xPosition][cell.yPosition];
        if (cell.isFlagged())  return sprite.loadTexture("flagged");
        if (!cell.isOpened())  return sprite.loadTexture("closed"); else {
            if (cell.isEmpty())
                return sprite.loadTexture("opened");
            if (cell.isMined())    return sprite.loadTexture("mined");

            return sprite.loadTexture("t" + cell.getNearbyMinesCount());
        }

    };

    var buildSprites = function () {

        for (var i = 0; i < cells.length; i++)
            for (var j = 0; j < cells[i].length; j++) {
                createCellSprite(cells[i][j]);              // building sprites
            }

        // placing the group in the middle of the canvas
        this.x = gameContext.width  / 2;
        this.y = gameContext.height / 2;
        this.pivot.set(this.width / 2, this.height / 2);
    };
    buildSprites = buildSprites.bind(this);
    buildSprites();
};

CreateMineFieldView.prototype = Object.create(Phaser.Group.prototype);
CreateMineFieldView.prototype.constructor = CreateMineFieldView;

/**
 * @param gameContext
 * @returns {*}
 * @constructor
 */
CreateWindow = function (gameContext) {
    Phaser.Group.call(this,gameContext, gameContext.world);

    var sprite = gameContext.add.sprite(0,0,        //sprite is used as background
        gameOptions.windowBackgroundSpriteName);

    sprite.height = gameOptions.windowHeight;
    sprite.width  = gameOptions.windowWidth;
    sprite.anchor.set(0.5);
    this.add(sprite);

    this.x = gameContext.width/2;                   //set this group at center of screen
    this.y = gameContext.height/2;

}
CreateWindow.prototype = Object.create(Phaser.Group.prototype);
CreateWindow.prototype.constructor = CreateWindow;

/**
 * Creates a slider component. This extends Phaser.Group
 * Holds digital display that display 2 digits and label to explain
 * meaning of value. Size of this component can be set-up in gameConfig
 * onValueChanged callback must return actual value to display.

 * (for example:
 * value of bound parameter was increased and callback was called
 * callback function checks if parameter's value can be increased, increase it and returns
 * actual value that will be displayed)
 *
 * @param gameContext Phaser.Game
 * @param onValueChangedCallback
 * @param callbackContext
 * @param labelWidth
 * @param label
 * @constructor
 */
CreateSlider = function (gameContext,onValueChangedCallback,callbackContext, labelWidth, label){
    Phaser.Group.call(this,gameContext, gameContext.world);
    var digitsCount           = 2;          //TODO:Expose as parameter or create setter

    var display = new CreateDigitalDisplay(gameContext,                       // create display
        gameOptions.shortButtonWidth * gameOptions.digitalDisplayRatio,
        gameOptions.shortButtonHeight ,
        digitsCount,gameOptions.digitalDisplaySpriteName);

    var onValueChanged = function (delta) {
        display.setValue(onValueChangedCallback.call(callbackContext,delta)); // calling a setter and display what it
    }                                                                         // returns

    display.x = gameOptions.shortButtonWidth + labelWidth;


    var decreaseButton = gameContext.add.button(                              // constructing button
        labelWidth,0,gameOptions.shortButtonSpriteName,                       // from the left of display
        function(){onValueChanged(-1)},this,0,1,2,3                           // this button decreases the value
    );

    var increaseButton = gameContext.add.button(                              // constructing button from the right
        labelWidth + gameOptions.shortButtonWidth*(                           // this button for increase
        1 + digitsCount*gameOptions.digitalDisplayRatio),0,
        gameOptions.shortButtonSpriteName,
        function(){onValueChanged(1)},this,0,1,2,3
    );

    increaseButton.width  = gameOptions.shortButtonWidth;
    increaseButton.height = gameOptions.shortButtonHeight;
    decreaseButton.width  = gameOptions.shortButtonWidth;
    decreaseButton.height = gameOptions.shortButtonHeight;

    var labelObject = gameContext.add.text(0,0,label);
    labelObject.anchor.set(0,-0.5);

    var prev = gameContext.add.sprite(0,0,"prev");
    var next = gameContext.add.sprite(0,0,"next");

    next.width = gameOptions.shortButtonWidth   *0.5; // 0.5 is for padding
    next.height = gameOptions.shortButtonHeight *0.5; // it will be good to add

    prev.width = gameOptions.shortButtonWidth   *0.5; //TODO: support for paddings
    prev.height = gameOptions.shortButtonHeight *0.5;

    increaseButton.addChild(next);
    decreaseButton.addChild(prev);

    this.add (labelObject);     //adding all constructed objects
    this.add(increaseButton);   //to group
    this.add(display);
    this.add(decreaseButton);
    onValueChanged(0);
}
CreateSlider.prototype = Object.create(Phaser.Group.prototype);
CreateSlider.prototype.constructor = CreateSlider;

/**
 * Creates window with game settings
 * @param gameContext
 * @param startNewGameCallback
 * @param goBackCallback
 * @param callbackContext
 * @constructor
 */
CreateConfigMenu = function (gameContext, startNewGameCallback, goBackCallback, callbackContext) {
    CreateWindow.apply(this,arguments);
    var yOffset = -250;
    var xOffset = -164;

    this.onMinesValueChanged = function(delta) { //this function is directly controls game settings
       var value = gameOptions.minesRatio + delta*0.01;
       value = gameContext.math.clamp(value,gameOptions.minesRatioMin, gameOptions.minesRatioMax);
       gameOptions.minesRatio = value;
       return value * 100;
    }

    this.onWidthValueChanged = function(delta) { //this function is directly controls game settings
        var value = gameOptions.fieldWidth + delta;
        value = gameContext.math.clamp(value,gameOptions.minFieldHeight, gameOptions.maxFieldWidth);
        gameOptions.fieldWidth = value;
        return value;
    }

    this.onHeightValueChanged = function(delta) { //this function is directly controls game settings
        var value = gameOptions.fieldHeight + delta;
        value = gameContext.math.clamp(value,gameOptions.minFieldHeight, gameOptions.maxFieldHeight);
        gameOptions.fieldHeight = value;
        return value;
    }

    //Create buttons--------------------------------------------------
    this.newGameButton =  gameContext.add.button (                  // this is hell
        - gameOptions.wideButtonWidth*0.5,                          // I've thought alot about create some simple
        yOffset + 32,                                               // editor and add support for some kind of JSON
        gameOptions.wideButtonSpriteName,                           // UI parser to be able to make layout files in
        startNewGameCallback,callbackContext,0,1,2,3);              // a way that it is done in XAML

    this.newGameButton.width  = gameOptions.wideButtonWidth;
    this.newGameButton.height = gameOptions.wideButtonHeight;

    this.backButton = gameContext.add.button (
         - gameOptions.wideButtonWidth*0.5,
        yOffset +336,
        gameOptions.wideButtonSpriteName,
        goBackCallback,callbackContext,0,1,2,3);

    this.backButton.width  = gameOptions.wideButtonWidth;
    this.backButton.height = gameOptions.wideButtonHeight;


    var text = gameContext.add.text(0,0,"Start New Game");
    text.setScaleMinMax(1.1,1.1);
    text.anchor.set(0,-1);
    text.x = 5;

    var textBack = gameContext.add.text(0,0,"Back");
    textBack.anchor.set(0,-1);
    textBack.x = 50;
    textBack.setScaleMinMax(1.1,1.1);

    this.newGameButton.addChild (text);
    this.backButton.addChild    (textBack);

    this.minesCountSlider = new CreateSlider(
        gameContext,this.onMinesValueChanged, this,164,"Mines (%):");

    this.rowsCountSlider = new CreateSlider(
        gameContext,this.onHeightValueChanged, this,164,"Rows:");

    this.colsCountSlider = new CreateSlider(
        gameContext,this.onWidthValueChanged, this,164,"Cols:");
    //-------------------------------------------------------------

    this.rowsCountSlider.y  = 160 + yOffset;
    this.colsCountSlider.y  = 208 + yOffset;
    this.minesCountSlider.y = 256 + yOffset;

    this.rowsCountSlider.x  = xOffset;
    this.colsCountSlider.x  = xOffset;
    this.minesCountSlider.x = xOffset;

    this.add(this.minesCountSlider);
    this.add(this.rowsCountSlider);
    this.add(this.colsCountSlider);
    this.add(this.newGameButton);
    this.add(this.backButton);

}
CreateConfigMenu.prototype = Object.create(CreateWindow.prototype);
CreateConfigMenu.prototype.constructor = CreateConfigMenu;

CreateToolbar = function (gameContext, onSettingsClick, onEnableBotClick, callbackContext) {
    Phaser.Group.call(this,gameContext, gameContext.world);

    var flagsCountDisplay = new CreateDigitalDisplay(gameContext,gameOptions.timerWidth,gameOptions.timerHeight,
        gameOptions.timerDigits,gameOptions.digitalDisplaySpriteName);

    var timeCountDisplay   = new CreateDigitalDisplay(gameContext,gameOptions.timerWidth,gameOptions.timerHeight,
        gameOptions.timerDigits,gameOptions.digitalDisplaySpriteName);

    timeCountDisplay.x = gameOptions.timerWidth*
        gameOptions.timerDigits + gameOptions.botButtonWidth + gameOptions.menuButtonWidth;

    var settingsButton = gameContext.add.button(gameOptions.timerWidth * gameOptions.timerDigits, 0,"options",
        onSettingsClick, callbackContext,0,1,2,3);

    var botButton = gameContext.add.button(gameOptions.timerWidth * gameOptions.timerDigits
        + gameOptions.menuButtonWidth, 0,"bot",
        onEnableBotClick, callbackContext,0,1,2,3);

    settingsButton.width = gameOptions.menuButtonWidth;
    settingsButton.height = gameOptions.menuButtonWidth;

    botButton.width = gameOptions.menuButtonWidth;
    botButton.height = gameOptions.menuButtonWidth;

    this.add (flagsCountDisplay);
    this.add (timeCountDisplay);
    this.add (settingsButton);
    this.add (botButton);
    this.x = gameContext.width/2 - gameOptions.timerWidth*3-gameOptions.menuButtonWidth;

    this.updateFlagsCount = function (num) {
        flagsCountDisplay.setValue(num)
    }

    this.updateTimerValue = function (value) {
        timeCountDisplay.setValue(value)
    }

}

CreateToolbar.prototype = Object.create(CreateWindow.prototype);
CreateToolbar.prototype.constructor = CreateToolbar;

/**
 * Creates a loose / win screen
 * @param gameContext
 * @param isWin (bool) if true creates win screen otherwise it show loose screen
 * @param time
 * @param mines
 * @param goBackCallback
 * @param callbackContext
 * @constructor
 */
CreateWinOrLooseScreen = function (gameContext, isWin, time, mines, goBackCallback, callbackContext) {
    CreateWindow.apply(this, arguments);
    var yOffset = -250;
    var win = gameContext.add.text(0,0,isWin ? "You Win!": "Game over!");
    win.anchor.set(0.5);
    this.add (win);
    win.y = -100;
    win.scale.set (2);

    var scoreText = gameContext.add.text(0,-25, "Your time: " + time);
    scoreText.anchor.set(0.5,0.5);
    this.add (scoreText);


    var minesText = gameContext.add.text(0,25, "Mines defused: " + mines);
    minesText.anchor.set(0.5,0.5);
    this.add (minesText);

    var backButton = gameContext.add.button (
        - gameOptions.wideButtonWidth*0.5,
        yOffset +336,
        gameOptions.wideButtonSpriteName,
        goBackCallback,callbackContext,0,1,2,3);

    backButton.width  = gameOptions.wideButtonWidth;
    backButton.height = gameOptions.wideButtonHeight;

    var textBack = gameContext.add.text(0,0,"Back");
    textBack.anchor.set(0,-1);
    textBack.x = 50;
    textBack.setScaleMinMax(1.1,1.1);

    backButton.addChild(textBack);
    this.add(backButton);

}
CreateWinOrLooseScreen.prototype = Object.create(CreateWindow.prototype);
CreateWinOrLooseScreen.prototype.constructor = CreateWinOrLooseScreen;


